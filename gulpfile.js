"use strict"

const { src, dest } = require("gulp"); 
const gulp = require("gulp"); 

const autoprefixer = require("gulp-autoprefixer");
const cssbeautify = require("gulp-cssbeautify");
const removeComments = require("gulp-strip-css-comments");
const rename = require("gulp-rename");
const sass = require("gulp-sass")(require('sass'));
const cssnano = require("gulp-cssnano");
const rigger = require("gulp-rigger");
const uglify = require("gulp-uglify");
const plumber = require("gulp-plumber");
const imagemin = require("gulp-imagemin");
const del = require("del");
const browserSync = require("browser-sync").create();
const notify = require("gulp-notify");
const fileinclude = require("gulp-file-include");
const gr_m_quries = require("gulp-group-css-media-queries");
const newer = require("gulp-newer");
const imgmin_webp = require("imagemin-webp");
const webp = require("gulp-webp");
const gulp_webp_html_nosvg = require("gulp-webp-html-nosvg");
const gulp_replace = require("gulp-replace");


// 1. Пути к папкам

const srcPath = "src/";
const distPath = "dist/";

const path = {
    build: {
        html: distPath,
        css: `${distPath}css/`,
        js: `${distPath}js/`,
        img: `${distPath}img/`,
        // fonts: `${distPath}fonts/`,
    },
    src: {
        //** - вложенные папки
        html: `${srcPath}*.html`,
        // html: `${srcPath}**/*.html`,  // берем html-файлы из всех папок в src!!
        css: `${srcPath}scss/style.scss`,
        js: `${srcPath}js/app.js`,
        // js: `${srcPath}js/**/*.js`,   Если нужно брать все файлы из всех подпапок в JS-folder

        img: `${srcPath}img/**/*.{jpeg,jpg,png,gif,ico,webp}`,  //без пробелов до запятой!!!!
        img_svg: `${srcPath}img/**/*.svg`,
    },
    //обновление..
    watch: {
        html: `${srcPath}**/*.html`,
        css: `${srcPath}scss/**/*.scss`,
        js: `${srcPath}js/**/*.js`,
        img: `${srcPath}img/**/*.{jpeg,jpg,png,svg,gif,ico,webp}`,
    },
    // cleaning dist folder..
    clean: `./${distPath}`
};


// 2. Задания, которые буду участвовать в формировании результирующих
// заданий build и dev.

function html() {
    return src(path.src.html, { base: srcPath })
        .pipe(plumber()) // скрывает ошибки, ломающие проект
        .pipe(fileinclude()) 
        .pipe(gulp_replace(/@img\//g, 'img/')) //многострадальные картинки.. фиксим пути к ним!
        .pipe(gulp_webp_html_nosvg()) //встраиваем вебп вместо исходных картинок
        .pipe(dest(path.build.html))
        .pipe(browserSync.reload({stream: true}));
};

function css() {
    return src(path.src.css, { base: `${srcPath}scss/` })
        .pipe(plumber({
            errorHandler: function (err) {
                notify.onError({
                    title: "SCSS error",
                    message: "Error: <%= error.message %>"
                })(err);
                this.emit("end");
            }
        }))
        .pipe(sass({
				outputStyle: "expanded"
        }))
        .pipe(gr_m_quries())
        .pipe(autoprefixer({
				grid: true,
				overrideBrowserslist: ["last 3 versions"],
				cascade: true
			}))
        .pipe(cssbeautify())
        .pipe(dest(path.build.css))
        .pipe(cssnano({
            zindex: false,
            discardComments: {
                removeAll: true
            }
        }))
        .pipe(removeComments())
        .pipe(rename({
            suffix: ".min",
            extname: ".css",
        }))
        .pipe(dest(path.build.css))
        .pipe(browserSync.reload({stream: true}));    
}

function js() {
    return src(path.src.js, { base: `${srcPath}js/` })
        .pipe(plumber({
            errorHandler: function (err1) {
                notify.onError({
                    title: "JS error",
                    message: "Error: <%= error.message %>"
                })(err1);
                this.emit("end");
            }
        }))
        .pipe(rigger())
        .pipe(dest(path.build.js))
        .pipe(uglify())
        .pipe(rename({
            suffix: ".min",
            extname: ".js",
        }))
        .pipe(dest(path.build.js))
        .pipe(browserSync.reload({stream: true}));    
}

//--------------
function img() {
    return src(path.src.img)
        .pipe(src(path.src.img_svg))        // берем картинки свг..
        .pipe(dest(path.build.img))         // .. и добавляем их в билд
        .pipe(newer(path.build.img))    //обновили
        .pipe(webp())                   //запустили вебп
        .pipe(dest(path.build.img))     // результат вебп - в папку билд		
        .pipe(src(path.src.img))        //опять проверяем папку с исходными картинками
		.pipe(newer(path.build.img))    //прорверяем билд на обновление картинок
		.pipe(imagemin({            // запускаем минимизацию изображений
				progressive: true,
				svgoPlugins: [{ removeViewBox: false }],
				interlaced: true,
				optimizationLevel: 4
			}))
        .pipe(dest(path.build.img))     //результат минимизации - в папку билд
        // .pipe(src(path.src.img_svg))    // берем картинки свг..
        // .pipe(dest(path.build.img))     // .. и добавляем их в билд
        .pipe(browserSync.reload({ stream: true }));
        
}

function clean() {
    return del(path.clean);
}

function watch_all() {
    gulp.watch([path.watch.html], html);
    gulp.watch([path.watch.css], css);
    gulp.watch([path.watch.js], js);
    gulp.watch([path.watch.img], img);
}

function sync() {
    browserSync.init({
        server: {
            baseDir: `./${distPath}`
        }
    });
}


const build = gulp.series(clean, gulp.parallel(html, css, js, img));
const dev = gulp.parallel(watch_all, sync);

const res = gulp.parallel(build, dev);



exports.html = html; // Запускаем в консоли gulp html..
exports.css = css;
exports.js = js;
exports.img = img;
exports.build = build;
exports.clean = clean;
exports.default = res;  // Пишем в консоли gulp, и по дефолту запускается watch